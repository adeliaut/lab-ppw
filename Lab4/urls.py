from django.urls import rpath
from . import views
# from .views import home, about, edu, exp, skill, gb, cp

urlpatterns = [
    path('', views.home, name='home'),
    path('', views.about, name='aboutme-education')
    path('', views.edu, name='aboutme-education')
    path('', views.exp, name='experience-skill')
    path('', views.skill, name='experience-skill')
    path('', views.cp, name='contact')
    path('', views.gb, name='guestbook')
	
    # re_path(r'^home', home , name= "home"),
    # re_path(r'^aboutme-education', about ,name= "aboutme"),
    # re_path(r'^aboutme-education', edu ,name= "education"),
    # re_path(r'^experience-skill', exp ,name= "experience"),
    # re_path(r'^experience-skill', skill ,name= "skill"),
    # re_path(r'^guestbook', gb ,name= "guestbook"),
    # re_path(r'^contact', cp ,name= "contact"),
]
