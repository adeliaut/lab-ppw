from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def home(request):
    return render(request, "home.html")
	
def about(request):
    return render(request, "aboutme-education.html")
	
def edu(request):
    return render(request, "aboutme-education.html")
	
def exp(request):
    return render(request, "experience-skill.html")
	
def skill(request):
    return render(request, "experience-skill.html")
	
def cp(request):
    return render(request, "contact.html")
	
def gb(request):
    return render(request, "guestbook.html")
