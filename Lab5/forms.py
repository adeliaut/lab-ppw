from django import forms
from django.forms import ModelForm
from .models import schedulefield

class scheduleform(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(scheduleform, self).__init__(*args, **kwargs)
		
		self.fields['time'] = forms.DateTimeField( widget= forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}), label='Time', input_formats=['%d/%m/%Y %H:%M'], error_messages={'invalid': 'Format is invalid.'})
		
		self.fields['activity'].label = 'activity'
		self.fields['activity'].widget.attrs = {'class': 'form-control', 'placeholder': 'Enter the activity name'}
		self.fields['activity'].error_messages = {'max_length': 'The maximum character is 200'} 
		
		self.fields['place'].label = 'Place'
		self.fields['place'].widget.attrs = {'class': 'form-control', 'placeholder': 'Enter the activity place'}
		self.fields['place'].error_messages = {'max_length': 'The maximum character is 200'} 
		
		self.fields['category'].label = 'Category'
		self.fields['category'].widget.attrs = {'class': 'form-control', 'placeholder': 'Enter the activity category'}
		self.fields['category'].error_messages = {'max_length': 'The maximum character is 200'} 
		
	class Meta:
		model = schedulefield
		fields = ['time', 'activity', 'place', 'category']