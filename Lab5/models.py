from django import forms
from django.db import models
import datetime

class schedulefield(models.Model):
    time = models.DateTimeField()
    activity = models.CharField(max_length = 200)
    place= models.CharField(max_length = 200)
    category = models.CharField(max_length = 200)
	
    def __str__(self):
        return self.activity