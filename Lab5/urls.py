from django.urls import path
from . import views
from .views import schedulelist

urlpatterns = [
	path('', views.scheduleform, name='scheduleform'),
	path('', views.schedulelist, name='schedulelist'),
]
