from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.contrib import messages

from django.urls import reverse
from .forms import scheduleform
from .models import schedulefield
from django.http import HttpResponseRedirect
from django.shortcuts import render

def updateschedule(request):
	if request.method == "POST":
		form = scheduleform(request.POST or None)
		if form.is_valid():
			form.save()
	else:
		out = 'scheduleform.html'
		form = scheduleform()
		t = loader.get_template('scheduleform.html')
		return render(request, out, {'form': form})
	
def updateschedulelist(request):
	form = scheduleform(request.POST or None)
	if request.method == 'POST':
		time = request.POST['time']
		activity = request.POST['activity'] if request.POST['activity'] != '' else 'Anonymous'
		place = request.POST['place'] if request.POST['place'] != '' else 'Anonymous'
		category = request.POST['category']
		message = schedulefield(time=time, activity=activity, place=place, category=category)
		message.save()
		
		out = 'schedulelist.html'
		context_object_name = 'schedules'
		queryset = schedulefield.objects.all().values()
		result = {context_object_name: queryset}
		return render(request, out, result)
	
	else:
		model = schedulefield
		out = 'schedulelist.html'
		context_object_name = 'schedules'
		queryset = schedulefield.objects.all().values()
		result = {context_object_name: queryset}
		return render(request, out, result)
		
def deleteall(request):
	out = 'schedulelist.html'
	schedulefield.objects.all().delete()
	return render(request, out)