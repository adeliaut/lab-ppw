"""LabPPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
from Lab4.views import home as homepg
from Lab4.views import about as aboutpg
from Lab4.views import edu as edupg
from Lab4.views import exp as exppg
from Lab4.views import skill as skillpg
from Lab4.views import cp as cppg
from Lab4.views import gb as  gbpg
from Lab5.views import updateschedule as  schdl
from Lab5.views import updateschedulelist as  schdlst
from Lab5.views import deleteall as  deleteall

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^home', homepg, name="home"),
    re_path(r'^aboutme', aboutpg, name="about"),
    re_path(r'^education', edupg, name="edu"),
    re_path(r'^experience', exppg, name="exp"),
    re_path(r'^skill', skillpg, name="skill"),
    re_path(r'^contact', cppg, name="cp"),
    re_path(r'^guestbook', gbpg, name="gb"),
    re_path(r'^scheduleform', schdl, name="updateschedule"),
    re_path(r'^schedulelist', schdlst, name="updateschedulelist"),
    re_path(r'^deleteall', deleteall, name="deleteall"),
]
